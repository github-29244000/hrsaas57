import layout from '@/layout'
export default {
  path: '/echart ',
  component: layout,
  children: [{
    path: '',
    name: 'echart',
    component: () => import('@/views/echart'),
    meta: {
      title: '图表',
      icon: 'setting'
    }
  }]
}
