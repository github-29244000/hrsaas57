import layout from '@/layout'
export default {
  path: '/map',
  component: layout,
  children: [{
    path: '',
    name: 'map',
    component: () => import('@/views/map'),
    meta: {
      title: '地图',
      icon: 'setting'
    }
  }]
}
