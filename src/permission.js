import router from '@/router'
import store from './store'
import nprogress from 'nprogress'
import 'nprogress/nprogress.css'
const whiteList = ['/login', '/404']
router.beforeEach((to, from, next) => {
  // ③从vuex拿token
  nprogress.start()
  const token = store.state.user.token
  // 有token?

  if (token) {
    console.log(store.getters.userId)
    if (!store.getters.userId) {
      store.dispatch('user/getuserInfo')
    }
    if (to.path === '/login') {
      next('/')
    } else {
      next()
    }
  } else { // 无token
    if (whiteList.includes(to.path)) {
      next()
    } else {
      next('/login')
    }
  }
  nprogress.done()
})
