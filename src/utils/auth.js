// import Cookies from 'js-cookie'

// const TokenKey = 'vue_admin_template_token'

const KEY = 'hm-token'
export const setToken = token => localStorage.setItem(KEY, token)
export const getToken = () => localStorage.getItem(KEY)
export const removeToken = () => localStorage.removeItem(KEY)
