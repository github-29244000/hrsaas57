import request from '@/utils/request'
export const getdepartment = () => {
  return request({
    url: '/company/department'

  })
}

export const adddepartment = (data) => {
  return request({
    url: '/company/department',
    method: 'POST',
    data
  })
}
export const username = () => {
  return request({
    url: '/sys/user/simple'

  })
}
// 获取-部门详情
export const particulars = (id) => {
  return request({
    url: `/company/department/${id}`

  })
}
// 修改-部门
export const amendpartment = (data) => {
  return request({
    url: `/company/department/${data.id}`,
    method: 'PUT',
    data

  })
}
// 删除-部门
export const delpartment = (id) => {
  return request({
    url: `/company/department/${id}`,
    method: 'DELETE'
  })
}
