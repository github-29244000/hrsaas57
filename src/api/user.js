import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/sys/login',
    method: 'post',
    data
  })
}

export function getuserInfo() {
  return request({
    url: '/sys/profile',
    method: 'get'

  })
}

export function logout() {
  return request({
    url: '/vue-admin-template/user/logout',
    method: 'post'
  })
}
export const changePassword = (data) => {
  return request({
    url: '/sys/user/updatePass',
    method: 'PUT',
    data
  })
}
