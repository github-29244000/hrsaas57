
import request from '@/utils/request'
export const getrole = () => {
  return request({
    url: '/sys/role'
  })
}
export const addrole = (data) => {
  return request({
    url: '/sys/role',
    method: 'POST',
    data
  })
}
export const delrole = (id) => {
  return request({
    url: `/sys/role/${id}`,
    method: 'DELETE'

  })
}
// 修改-角色
export const editrole = (data) => {
  return request({
    url: `/sys/role/${data.id}`,
    method: 'PUT',
    data
  })
}
// 获取-分页查询角色列表
export const inquirerole = (params) => {
  return request({
    url: '/sys/role',
    params
  })
}
