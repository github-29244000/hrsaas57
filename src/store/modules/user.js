import { getToken, setToken, removeToken } from '@/utils/auth'
import { login, getuserInfo } from '@/api/user'

export default {
  namespaced: true,
  state: {
    token: getToken(),
    userInfo: {}
  },
  getters: {},
  mutations: {
    changToken(state, token) {
      state.token = token
      setToken(token)
    },
    loginOut(state) {
      state.token = null
      removeToken()
    },
    setuserInfo(state, userInfo) {
      state.userInfo = userInfo
    },
    userinfoOut(state) {
      state.userInfo = null
    }
  },
  actions: {
    async _login(store, velue) {
      const data = await login(velue)
      store.commit('changToken', data)
    },
    async getuserInfo(store) {
      const data = await getuserInfo()
      store.commit('setuserInfo', data)
    },
    userOut(store) {
      store.commit('userinfoOut')
      store.commit('loginOut')
    }
  }
}
